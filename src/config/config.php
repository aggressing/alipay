<?php
return [

	//合作身份者id，以2088开头的16位纯数字。
	'partner_id' => '2088xxxxxxxxxxxx',

	//卖家支付宝帐户。
	'seller_email' => 'xxx@xxx.xxx',

    // 安全检验码，以数字和字母组成的32位字符。
    'key' => 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',

    'cacert_path' => __DIR__ . '/key/cacert.pem',

    // 商户私钥。
	'private_key_path' => __DIR__ . '/key/private_key.pem',

	// 阿里公钥。
	'public_key_path' => __DIR__ . '/key/public_key.pem'
];
