<?php
namespace Weibenling\Alipay;

use Illuminate\Support\ServiceProvider;

class AlipayServiceProvider extends ServiceProvider
{

	/**
	 * boot process
	 */
	public function boot()
	{
		$this->publishes([
			__DIR__ . '/../../config/config.php' => config_path('weibenling-alipay.php'),
			__DIR__ . '/../../config/mobile.php' => config_path('weibenling-alipay-mobile.php'),
			__DIR__ . '/../../config/web.php' => config_path('weibenling-alipay-web.php'),
			__DIR__ . '/../../config/btrans.php' => config_path('weibenling-alipay-btrans.php'),
			__DIR__ . '/../../config/refund.php' => config_path('weibenling-alipay-refund.php')
		]);
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->mergeConfigFrom(__DIR__ . '/../../config/config.php', 'weibenling-alipay');
		$this->mergeConfigFrom(__DIR__ . '/../../config/mobile.php', 'weibenling-alipay-mobile');
		$this->mergeConfigFrom(__DIR__ . '/../../config/btrans.php', 'weibenling-alipay-btrans');
		$this->mergeConfigFrom(__DIR__ . '/../../config/refund.php', 'weibenling-alipay-refund');
		$this->mergeConfigFrom(__DIR__ . '/../../config/web.php', 'weibenling-alipay-web');

		$this->app->bind('alipay.mobile', function ($app)
		{
			$alipay = new Mobile\SdkPayment();

			$alipay->setPartner($app->config->get('weibenling-alipay.partner_id'))
				->setSellerId($app->config->get('weibenling-alipay.seller_email'))
				->setPrivateKeyPath($app->config->get('weibenling-alipay.private_key_path'))
				->setPublicKeyPath($app->config->get('weibenling-alipay.public_key_path'))
                ->setCacert($app->config->get('weibenling-alipay.cacert_path'))
				->setNotifyUrl($app->config->get('weibenling-alipay-mobile.notify_url'))
                ->setSignType($app->config->get('weibenling-alipay-mobile.sign_type'));

			return $alipay;
		});

		$this->app->bind('alipay.web', function ($app)
		{
			$alipay = new Web\SdkPayment();

			$alipay->setPartner($app->config->get('weibenling-alipay.partner_id'))
				->setSellerId($app->config->get('weibenling-alipay.seller_email'))
				->setKey($app->config->get('weibenling-alipay.key'))
                ->setCacert($app->config->get('weibenling-alipay.cacert_path'))
				->setExterInvokeIp($app->request->getClientIp())
                ->setSignType($app->config->get('weibenling-alipay-web.sign_type'))
                ->setNotifyUrl($app->config->get('weibenling-alipay-web.notify_url'))
                ->setReturnUrl($app->config->get('weibenling-alipay-web.return_url'));

			return $alipay;
		});

		$this->app->bind('alipay.btrans', function ($app)
		{
			$alipay = new Btrans\SdkPayment();

			$alipay->setPartner($app->config->get('weibenling-alipay.partner_id'))
				->setEmail($app->config->get('weibenling-alipay.seller_email'))
				->setPrivateKeyPath($app->config->get('weibenling-alipay.private_key_path'))
				->setPublicKeyPath($app->config->get('weibenling-alipay.public_key_path'))
                ->setCacert($app->config->get('weibenling-alipay.cacert_path'))
                ->setAccount($app->config->get('weibenling-alipay-btrans.account_name'))
                ->setSignType($app->config->get('weibenling-alipay-btrans.sign_type'))
				->setNotifyUrl($app->config->get('weibenling-alipay-btrans.notify_url'));

			return $alipay;
		});

        $this->app->bind('alipay.refund', function ($app)
		{
			$alipay = new Refund\SdkPayment();

			$alipay->setPartner($app->config->get('weibenling-alipay.partner_id'))
				->setEmail($app->config->get('weibenling-alipay.seller_email'))
				->setPrivateKeyPath($app->config->get('weibenling-alipay.private_key_path'))
				->setPublicKeyPath($app->config->get('weibenling-alipay.public_key_path'))
                ->setCacert($app->config->get('weibenling-alipay.cacert_path'))
				->setNotifyUrl($app->config->get('weibenling-alipay-refund.notify_url'))
                ->setSignType($app->config->get('weibenling-alipay-refund.sign_type'));

			return $alipay;
		});

	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return [
			'alipay.mobile',
			'alipay.web',
			'alipay.btrans',
			'alipay.refund'
		];
	}
}
