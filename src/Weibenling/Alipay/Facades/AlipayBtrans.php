<?php

namespace Weibenling\Alipay\Facades;

use Illuminate\Support\Facades\Facade;

class AlipayBtrans extends Facade
{

	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor()
	{
		return 'alipay.btrans';
	}
}